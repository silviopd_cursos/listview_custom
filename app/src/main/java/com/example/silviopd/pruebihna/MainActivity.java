package com.example.silviopd.pruebihna;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.silviopd.pruebihna.negocio.Cliente;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btnAgregar;
    ListView lv_listado;
    static MainActivity act;

    Adaptador adaptador;
    ArrayList<Cliente> listaClientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAgregar = (Button) findViewById(R.id.btnAgregar);
        lv_listado = (ListView) findViewById(R.id.lv_listar);

        listaClientes = new ArrayList<Cliente>();
        adaptador = new Adaptador(this, listaClientes);
        lv_listado.setAdapter(adaptador);

        act=this;

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearItemLista();
                adaptador.setListaDatos(listaClientes);
            }
        });
    }

    public void crearItemLista(){
        Cliente obj=new Cliente();
        obj.setTxtDni("");
        listaClientes.add(obj);
    }
}
