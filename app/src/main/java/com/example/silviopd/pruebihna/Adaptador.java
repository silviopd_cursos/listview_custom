package com.example.silviopd.pruebihna;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.silviopd.pruebihna.negocio.Cliente;

import java.util.ArrayList;

public class Adaptador extends BaseAdapter{

    public static ArrayList<Cliente> listaDatos;
    private LayoutInflater layoutInflater;

    public static ArrayList<Cliente> getListaDatos() {
        return listaDatos;
    }

    public void setListaDatos(ArrayList<Cliente> listaDatos) {
        Adaptador.listaDatos = listaDatos;
        notifyDataSetChanged();
    }

    public Adaptador(Context con, ArrayList<Cliente> cli) {
        this.layoutInflater=LayoutInflater.from(con);
        this.listaDatos=cli;
    }

    @Override
    public int getCount() {
        return listaDatos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaDatos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final holder h;
        if (convertView==null){
            convertView=layoutInflater.inflate(R.layout.item_acomp,null);
            h=new holder();
            h.txtDni =(EditText) convertView.findViewById(R.id.txtDni);
            h.btneliminar = (Button) convertView.findViewById(R.id.btnquitar);
            h.btnleer = (Button) convertView.findViewById(R.id.btnleer);
            convertView.setTag(h);
        }else{
            h=(holder)convertView.getTag();
        }

        h.btneliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaDatos.remove(position);
                notifyDataSetChanged();
            }
        });

        h.btnleer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.act,h.txtDni.getText(),Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    static class holder{
        EditText txtDni;
        Button btneliminar;
        Button btnleer;
    }
}
